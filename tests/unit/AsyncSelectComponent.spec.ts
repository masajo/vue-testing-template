import { DOMWrapper, mount } from "@vue/test-utils";
import '@testing-library/jest-dom';

import AsyncSelectComponent from '@/components/AsyncSelectComponent.vue';

import { Country, COUNTRIES } from '../unit/__mocks__/Countries.mock';


// TODO: Jest Mock Fetch to obtain list of countries (spyOn)
// It must return:
/**
 * {
 *  data: {
 *      countries: COUNTRIES
 * }
 */


describe('AsyncSelectComponent.vue', () => {

    let wrapper = mount(AsyncSelectComponent);

    beforeEach(() => {
        wrapper = mount(AsyncSelectComponent);
    });

    xdescribe('Renders correctly and does async request', () => {

        // TODO: Check HTML of AsyncSelectComponent
        expect(wrapper.find('[data-countries]').exists()).toBe(true);

    })

    xdescribe('Listing Options', async () => {

        // TODO: Jest SpyOn "obtainCountries" from Services

        // TODO: Check than it has been called at leas once

        // TODO: Ensure DOM is updated

        // TODO: Search for the <select> tag and verify it exists
        const selectCountries: DOMWrapper<HTMLOptionElement> = wrapper.find('[data-countries]');
        expect(selectCountries.exists()).toBe(true);

        const options: DOMWrapper<HTMLOptionElement>[] = wrapper.findAll('[data-countries] option');

        // TODO: Verify there are TWO options (length toBe 2)
        expect(options.length).toBe(2);

        // TODO: Search <option> [0] have value/text COUNTRIES[0].name / id
        expect(options[0].text()).toBe(COUNTRIES[0].name);

        // TODO: Search <option> [2] have value/text COUNTRIES[1].name / id
        expect(options[1].text()).toBe(COUNTRIES[1].name);

        // TODO: Using Jest-DOM
        // using Jest-Dom
        expect(selectCountries).toHaveDisplayValue([COUNTRIES[0].name, COUNTRIES[1].name]);

    });


    describe('Selecting options', () => {

        xit('Select Country "España"', () => {

            // Trigger On Change
            // wrapper.findAll('[data-countries] option').at(0)?.trigger('change');

        });

    });


})
