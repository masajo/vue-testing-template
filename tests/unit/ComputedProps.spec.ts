import { mount } from "@vue/test-utils";
import ComputedProps from '@/components/ComputedProps.vue';


describe('ComputedProps.vue', () => {


    xdescribe('Even Numbers', () => {

        let wrapper: any = null;

        beforeEach(() => {
            wrapper = mount(ComputedProps, {
                props: {
                    limit: 10,
                    even: true
                }
            });
        })

        it('Renders Even Numbers correctly', () => {
            expect(wrapper.text()).toBe("2, 4, 6, 8, 10");
        });

       

    })


    xdescribe('Odd Numbers', () => {

        let wrapper: any = null;

        beforeEach(() => {
            wrapper = mount(ComputedProps, {
                props: {
                    limit: 10,
                    even: false
                }
            });
        })

        it('Renders Odd Numbers correctly', () => {
            expect(wrapper.text()).toBe("1, 3, 5, 7, 9");
        });

       

    })





})