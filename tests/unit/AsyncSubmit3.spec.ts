import { mount } from "@vue/test-utils";
import flushPromises  from "flush-promises";
import AsyncSubmit from '@/components/AsyncSubmit.vue';
import mockAxiosAdapter from '../unit/__mocks__/axios-adapter.mock';

const BASE_URL = 'https://reqres.in/api';

describe('AsyncSubmit.vue Mocked HTTP requests with MOCK ADAPTER', () => {

    afterEach(() => {
        mockAxiosAdapter.reset();
    })

    xit('Successful Login form shows a welcome message', async () => {

        // TODO: Using JEST-MOCK-AXIOS simulate HTTP Requests

        // ACT
        // Action in component that makes axios.post

        // Assert that the clla has been done
        expect(mockAxiosAdapter.history.get[0].url).toEqual(`${BASE_URL}/login`)

    });

    xdescribe('Api call fails', () => {

        it('Should receive an empty list of users', () => {
            
            mockAxiosAdapter.onGet(`${BASE_URL}/users`).networkErrorOnce();

            // ACT
            // Action in component that asks for users

            // Assert
            expect(mockAxiosAdapter.history.get[0].url).toEqual(`${BASE_URL}/users`);
            // assert the component value for the list of users is empty

        })


    });



})


