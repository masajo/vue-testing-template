import { shallowMount } from '@vue/test-utils'
import HelloWorld from '@/components/HelloWorld.vue'

// Describe => Test Suite (Conjunto de test cases)
describe('HelloWorld.vue', () => {
  
  // it => Test Case (Una prueba UNITARIA)
  it('renders props.msg when passed', () => {

    const msg = 'new message';

    const wrapper = shallowMount(HelloWorld, {
      props: { 
        msg 
      }
    })

    expect(wrapper.text()).toMatch(msg)

  });

})



/**
 * 
 * 1. Context
 * 2. Act
 * 3. Verify
 * 
 * RITE => Readable, Isolated/Integrated, Through, Explicit
 * 
 * SUT => System Under Tests
 * 
 */
