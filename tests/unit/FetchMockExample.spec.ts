/**
 * 
 * There are two options
 * 1. Usando global con jest.fn()
 * 2. Usando SpyOn
 * 
 */

import { myfunction } from "@/services/fetchService";

declare let global: any;


/**
 * Opción 1 - Usando global
 */
global.fetch = jest.fn(() => {
    Promise.resolve({
        data: {
            token: 'QpwL5tke4Pnpja7X4'
        }
    });
}) as jest.Mock;


/**
 * Opción 2 - Usando SpyOn
 */
const fetchSpyMock = jest.spyOn(global, 'fetch').mockImplementation(
    jest.fn(() => {
        Promise.resolve({
            data: {
                token: 'QpwL5tke4Pnpja7X4'
            }
        });
    }) as jest.Mock);


describe('Fetch tests', () => { 
    it('Fetch is called with text "Hola"', async () => {
        // ACT --> Somtehing that executes fetch
        await myfunction(true);
        // Verification -> Check that fetch has been called
        await expect(fetchSpyMock).toHaveBeenCalled();
        await expect(fetchSpyMock).toHaveBeenCalledTimes(1);
        await expect(fetchSpyMock).toHaveBeenCalledWith('hola');
        // Restore Mock
        await fetchSpyMock.mockReset(); // reset the mock
        await fetchSpyMock.mockRestore(); // restore the mock
    });

    it('Fetch is not called', async () => {
        // ACT --> Somtehing that executes fetch
        await myfunction(false);
        // Verification -> Check that fetch has been called
        await expect(fetchSpyMock).not.toHaveBeenCalled();
        // Restore Mock
        await fetchSpyMock.mockReset(); // reset the mock
        await fetchSpyMock.mockRestore(); // restore the mock
    });
    
 });
