import { mount } from "@vue/test-utils";
import Greeting2 from '@/components/Greeting2.vue';

describe('Greeting2.vue tests', () => {

    it('Component renders correctly the greeting text', () => {

        // Define Wrapper
        const wrapper = mount(Greeting2);

        // We print the component's HTML template
        console.log(wrapper.html());

        // We verify that the HTML includes the text: "Hello, Martín"
        expect(wrapper.html().includes("Hello, Martín")).toBe(true);
        expect(wrapper.find('div').text()).toBe('Hello, Martín');

    })

})
