import { mount } from "@vue/test-utils";
import EmitterEvents from '@/components/EmitterEvents.vue';


describe('EmitterEvents.vue emits events', () => {

    xit('The component emits an event', () => {

        const wrapper = mount(EmitterEvents);

        // Execution of method that emits an event
        wrapper.vm.emitEvent();

        // Control if the component has emitted something
        expect(wrapper.emitted()).toBeTruthy();

    });

    xit('The components emits a contoled event', () => {

        const wrapper = mount(EmitterEvents);

        // Execution of method that emits an event
        wrapper.vm.emitEvent();

        expect(wrapper.emitted().myEvent).toBeTruthy();

    });

    xit('The components emits a contoled event with controlled values', () => {

        const wrapper = mount(EmitterEvents);

        // Execution of method that emits an event
        wrapper.vm.emitEvent();

        // We check that the component emits "['name', 'password']"
        expect(wrapper.emitted().myEvent[0]).toEqual(['name', 'password']);

    });

    // We can use CALL to simulate the execution of a component's function
    xit('Emit event without mount of component', () => {

        const events: any = {};

        const $emit = (event: any, ...args:any) => {
            events[event] = [...args]
        }

        /**
         * We use call to invoke emitEvent method in component
         */
        EmitterEvents!.methods!.emitEvent.call({ $emit });

        console.log(events)

        // Check that it emits ['name', 'password'] inside the method
        expect(events.myEvent).toEqual(['name', 'password']);

    });

});


/**
 * 
 * Event emitting
 * 
 * - emitted() method to check the component emits something
 * - $emit +  call => Mock the emit() method of the component and do a call() 
 *  ---> Assert without renderization of the component (much faster)
 * 
 */