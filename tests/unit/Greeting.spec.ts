import { mount } from "@vue/test-utils";
import Greeting from '@/components/Greeting.vue';

describe('Greeting.vue tests', () => {

    it('Component renders correctly the greeting text', () => {

        // Define Wrapper
        const wrapper = mount(Greeting);

        // We print the component's HTML template
        console.log(wrapper.html());

        // We verify that the HTML includes the text: "Hello, Martín"
        expect(wrapper.html().includes("Hello, Martín")).toBe(true);
        expect(wrapper.find('div').text()).toBe('Hello, Martín');

    })



})
