

describe('1. Before & After Examples', () => {

    let contact: any = null;

    beforeEach(() => {
        console.log('1. Before Each');
        contact = {
            name: 'Martín',
            email: 'martin@imaginagroup.com'
        }
    });

    afterEach(() => {
        console.log('1. After Each');
        contact = null;
    });

    beforeAll(() => {
        console.log('1. Before All');
        contact = {
            name: 'Eric',
            email: 'eric@imaginagroup.com'
        }
    });

    afterAll(() => {
        console.log('1. After All');
        contact = null;
    });

    it('Contact should be Martín', () => {

        console.log('1. *TEST*');
        expect(contact).toStrictEqual({
            name: 'Martín',
            email: 'martin@imaginagroup.com'
        });

    });

    describe('2. New Test Suite', () => {

        beforeEach(() => {
            console.log('2. Before Each');
            contact = {
                name: 'Oscar',
                email: 'oscar@imaginagroup.com'
            }
        });

        afterEach(() => {
            console.log('2. After Each');
            contact = null;
        });

        beforeAll(() => {
            console.log('2. Before All');
            contact = {
                name: 'Ivan',
                email: 'ivan@imaginagroup.com'
            }
        });

        afterAll(() => {
            console.log('2. After All');
            contact = null;
        });


        it('Contact should be Óscar', () => {

            console.log('2. *TEST*');
            expect(contact).toStrictEqual({
                name: 'Oscar',
                email: 'oscar@imaginagroup.com'
            });
    
        });

    });

});


/**
 * Expected ORDER:
 * 
 * - 1. Before & After Examples (describe)
 * - 1. Before All
 * - 1. Before Each
 * - 1. *TEST*
 * - 1. After Each
 * - 2. Before All
 * - 2. New Test Suite (describe)
 * - 1. Before Each (another time!)
 * - 2. Before Each
 * - 2. *TEST*
 * - 2. After Each
 * - 1. After Each (another time!)
 * - 2. After All
 * - 1. After All (another time!)
 * 
 */