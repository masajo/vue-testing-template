import { obtainPromise, obtainRandomContact } from "@/services/userService";
import { AxiosResponse } from "axios";

/**
 * 
 * MOCK de AXIOS de User Service
 * 
 */

describe('Async examples', () => {

    // Obtain Result
    it('Obtain asynchronous result', () => { 
        return obtainRandomContact()
            .then(
                (response: AxiosResponse) => {

                    console.log('Response:', response);

                    expect(response).toBeDefined();
                    expect(response.data).toBeDefined();
                    expect(response.status).toBe(200);
                }
            )
    });

    // Catch an error
    it('Catch an error', () => {
        return obtainRandomContact()
            .catch((error: any) => {
                expect(error).toMatch(/Error:/);
            });
    });

    // Resolves 
    it('Promise Resolves correctly', () => {
        return expect(obtainPromise(true)).resolves.toBe('Correct Value');
    });

    // Rejects
    it('Promise Rejects', () => {
        return expect(obtainPromise(false)).rejects.toBe('Incorrect Value');
    });

});