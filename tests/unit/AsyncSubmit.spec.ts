import { mount } from "@vue/test-utils";
import flushPromises  from "flush-promises";
import AsyncSubmit from '@/components/AsyncSubmit.vue';


/**
 * MOCK AXIOS LIBRARY
 * 
 * So any component that uses axios, will be mocked in these tests
 * 
 * In this mock, we will simulate POST requests
 */

jest.mock('axios', () => (
    {
        post: () => Promise.resolve(
            {
                data: {
                    token: 'QpwL5tke4Pnpja7X4'
                }
            }
        )
    }
));


describe('AsyncSubmit.vue Mocked HTTP requests', () => {

    xit('Successful Login form shows a welcome message', async () => {

        // ARRANGE
        const wrapper = mount(AsyncSubmit);

        // ACT
        await wrapper.find('[data-email]').setValue('eve.holt@reqres.in');
        await wrapper.find('[data-password]').setValue('cityslicka');
        await wrapper.find('form').trigger('submit.prevent');

        // Flush the promise of the mocked HTTP request
        await flushPromises();

        // ASSERTS
        expect(wrapper.vm.token).toBe('QpwL5tke4Pnpja7X4'); // Access to Data from the component
        expect(wrapper.vm.$data.token).toBe('QpwL5tke4Pnpja7X4'); // Access to Data from the component
        expect(wrapper.find('.message')).toBeTruthy(); // Check the message is rendered
        expect(wrapper.find('.message').text()).toContain('QpwL5tke4Pnpja7X4'); // Check the message contains the token

        // Component must show: Welcome (eve.holt@reqres.in). Your token is: (QpwL5tke4Pnpja7X4)


    });

})


