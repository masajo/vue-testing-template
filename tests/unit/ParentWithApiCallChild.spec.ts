import { mount, shallowMount } from "@vue/test-utils";
import ParentWithApiCallChild from '@/components/ParentWithApiCallChild.vue';
import ChildWithApiCall from '@/components/ChildWithApiCall.vue'

describe('ParentWithApiCallChild.vue', () => {

    xit('1. Renders correctly the Child component inside (MOUNT)', () => {

        const wrapper = mount(ParentWithApiCallChild);

        // TODO: Use console.log in API CALL inside Child Component to show if the API Call is done
        
        // ? In this case the API CALL in ChildWithApiCall IS Executed

        /**
         * We use findComponent() to assert the existence of ChildWithApiCall
         */
        expect(wrapper.findComponent(ChildWithApiCall).exists()).toBe(true);

    });

    xit('2. Renders correctly the Child component inside, but does not execute the API Call (with MOUNT)', () => {

        const wrapper = mount(ParentWithApiCallChild, {
            stubs: {
                ChildWithApiCall: true
            }
        });

        // ? In this case the API CALL in ChildWithApiCall IS NOT Executed

        /**
         * We use findComponent() to assert the existence of ChildWithApiCall
         */
         expect(wrapper.findComponent(ChildWithApiCall).exists()).toBe(true);

    });

    xit('3. Renders correctly the Child component inside (SHALLOW MOUNT)', () => {

        const wrapper = shallowMount(ParentWithApiCallChild);

        // ? In this case the API CALL in ChildWithApiCall IS NOT Executed

        /**
         * We use findComponent() to assert the existence of ChildWithApiCall
         */
         expect(wrapper.findComponent(ChildWithApiCall).exists()).toBe(true);

    });

});

