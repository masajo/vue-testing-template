import { mount, shallowMount } from "@vue/test-utils";
import Parent from '@/components/Parent.vue';

describe('Parent.vue tests', () => {

    it('Renders Parent component successfully', () => {

        // Mounting Examples
        const wrapper = mount(Parent);
        const shallowWrapper = shallowMount(Parent);

        // Show HTML in both wrappers
        console.log(wrapper.html()); // shows the component as it is
        console.log(shallowWrapper.html()); // shows the component with STUB of CHILD <child-stub>

        // RESUMEN de mount() vs shallowMount()
        /**
         * - Como regla general hay que intentar usar mount()
         *  - ya que se parecerá más al componente real
         * - Si hay problemas de tiempo, peticiones a APIs, etc.
         *  - Estas dependencias se deben simular para que no afecten al test del SUT
         *  - Es entonces cuando se puede hacer un "montaje superficial" simulando el resto de componentes   
         */

        // TODO: IMPLEMENT MORE TESTS


    })

})
