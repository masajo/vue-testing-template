import { mount } from "@vue/test-utils";
import UserInput from '@/components/UserInput.vue';

xdescribe('UserInput.vue', () => {

    it('Renders message to user', async () => {

        // ARRANGE
        const wrapper = mount(UserInput);

        // ACT
        await wrapper.find("[data-username]").setValue('Martín');

        await wrapper.vm.$nextTick(); // WAIT UNTIL THE DOM IS UPDATED

        await wrapper.find('form').trigger("sumbit.prevent");

        // ASSERTS
        expect(wrapper.find(".message").text()).toBe("Thanks, Martín");


    });


})

