type AuthenticationData = {
    email:string,
    password: string
}

let url = '';
let body: AuthenticationData | undefined;
const mockAnError = false;

jest.mock('axios', () => ({
    post: (_url: string, _body:AuthenticationData) => {

        return new Promise((resolve, reject) => {
            if(mockAnError){
                reject('Error in API')
            }
            url = _url;
            body = _body;
            resolve(
                {
                    data: {
                        token: 'QpwL5tke4Pnpja7X4'
                    }
                }
            );
        });
    }
}));

describe('Axios mock example', () => {
    it('Axios example', () => {
        expect(true).toBe(true);
    })
})