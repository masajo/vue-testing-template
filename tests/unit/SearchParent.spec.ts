import { mount } from "@vue/test-utils";
import '@testing-library/jest-dom';
import SearchParent from '@/components/SearchParent.vue';
import SearchChild from '@/components/SearcChild.vue';

describe('SearchParent.vue', () => {

    xdescribe('Search DOM Elements', ()=> {

        it('<p> element is not visible', () => {
            const wrapper = mount(SearchParent);
            expect(wrapper.find('p').element).not.toBeVisible();
        });

        it('<p> element is visible when showP is true', () => {
            const wrapper = mount(SearchParent, {
                data() {
                    return {
                        showP: true
                    }
                }
            });

            expect(wrapper.find('p').element).toBeVisible();
            expect(wrapper.find('p').text()).toBe('Your childs');
        });


    });

    xdescribe('Search for 2 child components in DOM of Parent', ()=> {

        it('child components are visible when showP is true', () => {
            const wrapper = mount(SearchParent, {
                data() {
                    return {
                        showP: true
                    }
                }
            });
    
            expect(wrapper.findAllComponents(SearchChild).length).toBe(2);
        });

        it('At least one child component exists', () => {
            const wrapper = mount(SearchParent, {
                data() {
                    return {
                        showP: true
                    }
                }
            });
    
            expect(wrapper.findAllComponents(SearchChild).length).toBeGreaterThanOrEqual(1);
        });


        it('Another way to find components', () => {
            const wrapper = mount(SearchParent, {
                data() {
                    return {
                        showP: true
                    }
                }
            });
            
            expect(wrapper.findComponent(SearchChild).exists()).toBe(true);
            expect(wrapper.findComponent({name: "SearchChild"}).exists()).toBe(true);
        });

    });


});
