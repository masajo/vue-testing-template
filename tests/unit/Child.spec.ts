import { mount, shallowMount } from "@vue/test-utils";
import Child from '@/components/Child.vue';

describe('Child.vue tests', () => {

    xit('Renders Child component successfuly', () => {

        // Mounting Examples
        const wrapper = mount(Child);
        const shallowWrapper = shallowMount(Child);

        // Show HTML in both wrappers
        console.log(wrapper.html()); // same output
        console.log(shallowWrapper.html()); // same output

        // Expects
        expect(wrapper.html().includes('My Child Component')).toBe(true);
        expect(shallowWrapper.html().includes('My Child Component')).toBe(true);

        expect(wrapper.text()).toMatch('My Child Component');
        expect(shallowWrapper.text()).toMatch('My Child Component');
    })

})
