import { mount } from "@vue/test-utils";
import PropsComponent from '@/components/PropsComponent.vue';

describe('Tests to pass props to PropsComponent', () => {

    xit('Shows "Not authorized" text message', () => {

        const name = "Martín";

        const wrapper = mount(PropsComponent, {
            props: {
                name
            }
        });

        console.log(wrapper.html());

        // Expects
        expect(wrapper.find("span").text()).toBe('Martín, you are Not Authorized')
        expect(wrapper.find("button").exists()).toBeFalsy();
    });

    xit('Shows "Authorized" text message', () => {

        const name = "Martín";
        const hasRole = true;

        const wrapper = mount(PropsComponent, {
            props: {
                name,
                hasRole
            }
        });

        console.log(wrapper.html());

        // Expects
        expect(wrapper.find("span").text()).toBe('Martín, you are Authorized to send a message');
        expect(wrapper.find("button").exists()).toBeTruthy();
    });


})