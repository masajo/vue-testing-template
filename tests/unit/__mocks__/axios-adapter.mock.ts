import axios from 'axios';
import axiosMockAdapter from 'axios-mock-adapter';

const mockAxiosAdapter = new axiosMockAdapter(axios);

const BASE_URL = 'https://reqres.in/api';

mockAxiosAdapter.onGet(`${BASE_URL}/users`).reply(
    200, {
        users: [
            { id: 1, name: "John Smith" }
        ],
    }
);

mockAxiosAdapter.onPost(`${BASE_URL}/login`, {email: 'eve.holt@reqres.in', password: 'cityslicka'})
    .reply(
        200, {
           data: {
               token: 'QpwL5tke4Pnpja7X4'
           }
        }
);

export default mockAxiosAdapter;
