type Country = {
    id: string,
    name: string
}


const COUNTRIES: Country[] = [
    {
        id: 'ES',
        name: 'España'
    },
    {
        id: 'PT',
        name: 'Portugal'
    }
]

export { Country, COUNTRIES}