# Authetication Feature File (Example to understand Gherkin's Syntax)
@authetication
Feature: Authetication Fatures

    Scenarios to prove authetication requirements in our app

    # Previous Scenario ands steps before each scenario of feature
    Background: User is in Login Page
        # CONTEXT
        Given The user is in Login Page


    @login @successful
    Scenario: Login Successful

        # * STEPS *
        # CONTEXT
        # Given The user is in Login Page

        # ACT
        When The user types "Admin" in field username
        And The user types "admin123" in field password
        And The user submits the login form

        # ASSERTIONS
        Then The app navigates to Dashboard Page
        And the welcome message appears


    Scenario Outline: Login Unsuccessful
        # ACT
        When The user types <username> in field username
        And The user types <password> in field password
        And The user submits the login form

        # ASSERTIONS
        And the error message appears <errorMessage> in login form

        Examples:
            | username | password | errorMessage           |
            | Nimda1   | nimda321 | you're not welcome 321 |
            | Nimda2   | nimda432 | you're not welcome 432 |
            | Nimda3   | nimda657 | you're not welcome 657 |
