

describe('Fill form to obtain info', () => {

    beforeEach(() => {
        // Visit URL
        cy.visit('https://precom.airzonecloud.com/ib/es/confortairzone/calefaccion/');
        // Accept cookies
        // cy.get('#CybotCookiebotDialogBodyLevelButtonLevelOptinAllowAll').click();
    });

    it('Ask for info button exists', () => {
        cy.get('.ml-2 > .btn').click();
        cy.get('.d-block > :nth-child(1) > .btn').click();
        cy.get('.vs__open-indicator').click();
        cy.get('#vs1__combobox').click();
        cy.contains('Ferrolli').click();
        cy.get('#quantity0').select(4);
        cy.get('a > .btn').should('be.enabled');
        cy.get('a > .btn').click();

        // Fill form data
        cy.fillUserData('Oscar','Álvarez','oscar@imaginagroup.com','46021');
        // Accept terms & conditions form
        cy.get('.custom-control-label').click();

        // Submit the form
        cy.get('.btn').click();

        // Check Pathname of URL
        cy.location('pathname', { timeout: 60000}).should('include', '/ib/es');

        // Check button is present in timeuot
        cy.get('.nuxt-link-active', { timeout: 30000 }).should('be.visible').click();
       
    });

    // it('Find element by xPath', () => {

    //     const xpath = '\/\/*[@id="__layout"]/div/main/header/div[2]/div/div/div[2]/button'
    //     cy.xpath(xpath).click();
    // })

});



