// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })


// * Custom Command -> Fill User Info to obtain Product info
Cypress.Commands.add("fillUserData", (name,surname, email, cp ) => {
    cy.get('#name').type(name);
    cy.get('#surname').type(surname);
    cy.get('#email').type(email);
    cy.get('#cp').type(cp);
    cy.screenshot('form', {
        overwrite: true
    });
})


