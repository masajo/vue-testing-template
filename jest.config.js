module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  // Configuraciones manuales
  clearMocks: true,
  collectCoverage: true,
  coverageDirectory: "tests/unit/coverage",
  coveragePathIgnorePatterns: [
    "/node_modules/",
    "/tests/e2e"
  ],
  moduleDirectories: [
    "node_modules"
  ],
  moduleFileExtensions: [
    "js",
    "ts",
    "jsx",
    "tsx",
    "json",
    "node",
    "vue"
  ],
  testPathIgnorePatterns: [
    "/node_modules"
  ],

}
