import axios from "axios"

const obtainRandomContact = () => {
    return axios.get('https://randomuser.me/api');
}


const obtainPromise = (ok: boolean) => {
    return new Promise((resolve, reject) => {
        if(ok){
            resolve('Correct Value')
        }else{
            reject('Incorrect Value')
        }
    })
}


export { obtainRandomContact, obtainPromise }